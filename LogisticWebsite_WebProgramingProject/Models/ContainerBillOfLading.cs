﻿using System;
using System.Collections.Generic;

namespace LogisticWebsite_WebProgramingProject.Models;

public partial class ContainerBillOfLading
{
    public string? Container { get; set; }
}
