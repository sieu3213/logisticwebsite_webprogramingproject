﻿using System;
using System.Collections.Generic;

namespace LogisticsWebsite_WebProgramingProject.Models;

public partial class Schedule
{
    public int ScheduleId { get; set; }

    public int? ShipId { get; set; }

    public string Pol { get; set; } = null!;

    public string Pod { get; set; } = null!;

    public DateOnly DayGo { get; set; }

    public TimeOnly? TimeGo { get; set; }

    public DateOnly DayCome { get; set; }

    public virtual ICollection<BillOfLading> BillOfLadings { get; set; } = new List<BillOfLading>();

    public virtual Ship? Ship { get; set; }
}
